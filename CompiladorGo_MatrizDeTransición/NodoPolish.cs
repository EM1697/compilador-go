﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompiladorGo_MatrizDeTransición
{
    class NodoPolish
    {
        public NodoPolish(string lexema, string flag, int token)
        {
            this.lexema = lexema;
            this.flag = flag;
            this.token = token;
        }
        public string lexema;
        public string flag;
        public int token;
    }
}
