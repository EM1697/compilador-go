﻿namespace CompiladorGo_MatrizDeTransición
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.htmlToolTip1 = new MetroFramework.Drawing.Html.HtmlToolTip();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tablaTokens = new System.Windows.Forms.DataGridView();
            this.columnaLexema = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaToken = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaRenglon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSemantica = new MetroFramework.Controls.MetroButton();
            this.btnListaPolish = new MetroFramework.Controls.MetroButton();
            this.btnCompilar = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.tablaTokens)).BeginInit();
            this.SuspendLayout();
            // 
            // htmlToolTip1
            // 
            this.htmlToolTip1.OwnerDraw = true;
            // 
            // txtCodigo
            // 
            this.txtCodigo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodigo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.Location = new System.Drawing.Point(80, 94);
            this.txtCodigo.Multiline = true;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtCodigo.Size = new System.Drawing.Size(520, 275);
            this.txtCodigo.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Segoe MDL2 Assets", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(566, 65);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(34, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tablaTokens
            // 
            this.tablaTokens.AllowUserToDeleteRows = false;
            this.tablaTokens.AllowUserToOrderColumns = true;
            this.tablaTokens.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tablaTokens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablaTokens.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnaLexema,
            this.columnaToken,
            this.columnaRenglon});
            this.tablaTokens.Location = new System.Drawing.Point(80, 94);
            this.tablaTokens.Name = "tablaTokens";
            this.tablaTokens.Size = new System.Drawing.Size(520, 313);
            this.tablaTokens.TabIndex = 3;
            this.tablaTokens.Visible = false;
            // 
            // columnaLexema
            // 
            this.columnaLexema.HeaderText = "Lexema";
            this.columnaLexema.Name = "columnaLexema";
            this.columnaLexema.ReadOnly = true;
            // 
            // columnaToken
            // 
            this.columnaToken.HeaderText = "Token";
            this.columnaToken.Name = "columnaToken";
            this.columnaToken.ReadOnly = true;
            // 
            // columnaRenglon
            // 
            this.columnaRenglon.HeaderText = "Renglón";
            this.columnaRenglon.Name = "columnaRenglon";
            this.columnaRenglon.ReadOnly = true;
            // 
            // btnSemantica
            // 
            this.btnSemantica.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSemantica.Location = new System.Drawing.Point(80, 426);
            this.btnSemantica.Name = "btnSemantica";
            this.btnSemantica.Size = new System.Drawing.Size(112, 31);
            this.btnSemantica.TabIndex = 5;
            this.btnSemantica.Text = "Verificar Semántica";
            this.btnSemantica.UseSelectable = true;
            this.btnSemantica.Click += new System.EventHandler(this.btnSemantica_Click);
            // 
            // btnListaPolish
            // 
            this.btnListaPolish.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnListaPolish.Location = new System.Drawing.Point(286, 426);
            this.btnListaPolish.Name = "btnListaPolish";
            this.btnListaPolish.Size = new System.Drawing.Size(112, 31);
            this.btnListaPolish.TabIndex = 6;
            this.btnListaPolish.Text = "Lista de Polish";
            this.btnListaPolish.UseSelectable = true;
            this.btnListaPolish.Click += new System.EventHandler(this.btnListaPolish_Click);
            // 
            // btnCompilar
            // 
            this.btnCompilar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCompilar.Location = new System.Drawing.Point(488, 426);
            this.btnCompilar.Name = "btnCompilar";
            this.btnCompilar.Size = new System.Drawing.Size(112, 31);
            this.btnCompilar.TabIndex = 7;
            this.btnCompilar.Text = "Compilar";
            this.btnCompilar.UseSelectable = true;
            this.btnCompilar.Click += new System.EventHandler(this.btnCompilar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 480);
            this.Controls.Add(this.btnCompilar);
            this.Controls.Add(this.btnListaPolish);
            this.Controls.Add(this.btnSemantica);
            this.Controls.Add(this.tablaTokens);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtCodigo);
            this.Name = "Form1";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Text = "Lenguaje GO";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tablaTokens)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Drawing.Html.HtmlToolTip htmlToolTip1;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView tablaTokens;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaLexema;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaToken;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaRenglon;
        private MetroFramework.Controls.MetroButton btnSemantica;
        private MetroFramework.Controls.MetroButton btnListaPolish;
        private MetroFramework.Controls.MetroButton btnCompilar;
    }
}

