﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace CompiladorGo_MatrizDeTransición
{
    public partial class Form1 : MetroForm
    {
        #region Variables
        //Léxico
        int estado = 0;
        string code = System.IO.File.ReadAllText(@"C:\Users\Enrique\source\repos\compilador-go\Code.txt");
        int columna;
        int renglon = 1;
        int valorMT;
        char caracter;
        string cadena = "";
        int[,] matrizT = new int[17, 30];
        string[] palabrasReservadas = new string[22];
        string mensaje = " ";
        string titulo = "";
        int mesSem = 0;
        int tokenTipo;
        Nodo cabeza;
        Nodo p;
        List<string> pilaVariablesSemantica = new List<string>();
        List<Nodo> listaDeNodos = new List<Nodo>();
        string todosLosNodos;
        int i = 0;
        bool eof = false;
        string cadenaStringAux = "";

        //Sintaxis
        int contador = 0;
        int token = 0;
        int msg = 0;
        string tituloSintaxis = "";
        string mensajeSintaxis = "";
        string tituloSemantica = "";
        string mensajeSemantica = "";

        //Semántica
        Nodo pSemantica, cabezaSemantica;
        List<Nodo> listaDeVariables = new List<Nodo>();
        Stack<Nodo> pilaOperadores = new Stack<Nodo>();
        Stack<Nodo> pilaOperandos = new Stack<Nodo>();
        List<Nodo> listaSemanticaOperaciones = new List<Nodo>();
        List<Nodo> listaTemporalOperadores = new List<Nodo>();
        int tokenVariableDefinida;
        string lexemaMensaje;

        //ListaPolish
        List<NodoPolish> listaPolish = new List<NodoPolish>();
        string flagPolish = null;
        int contadorIf = 0;
        int contadorFor = 0;
        int tipoVariable;
        string verListaPolish = "";

        //Ensamblador
        Ensamblador asm = new Ensamblador();
        #endregion

        #region Form
        public Form1()
        {
            InitializeComponent();
            tablaTokens.Font = new Font("Tahoma", 12);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LlenarMatrizT();
            LlenasPalabrasreservadas();

            try
            {
                txtCodigo.Text = code;
            }
            catch (Exception)
            {

                MessageBox.Show(this, "Error al cargar el txt file de entrada", "Error de carga");
            } //Cargar txt file de entrada

        }
        #endregion

        #region Botones
        private void button1_Click(object sender, EventArgs e) //Botón Reload 
        {
            try
            {
                code = System.IO.File.ReadAllText(@"C:\Users\Enrique\source\repos\compilador-go\Code.txt");
                txtCodigo.Text = code;
                txtCodigo.Show();
                tablaTokens.Hide();
            }
            catch (Exception)
            {

                MessageBox.Show(this, "Error al actualizar el txt file", "Error de refresh");
            }
        }

        private void botLexico_Click_1(object sender, EventArgs e)
        {
            code = txtCodigo.Text;
            cabeza = null;
            p = null;
            listaDeNodos.Clear();
            //code += "";
            try
            {
                ValidarLéxico();
                //txtCodigo.Text = Imprimir();
                if (valorMT >= 500)
                {
                    MessageBox.Show(this, mensaje + " en renglón " + renglon.ToString() + " en: " + cadena, titulo, MessageBoxButtons.OK);
                    mensaje = "";
                    titulo = "";
                    cadena = "";
                    estado = 0;
                    valorMT = 0;
                }
                else
                {
                    Imprimir();
                }
                i = 0;
                todosLosNodos = null;
                renglon = 1;
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Algún error despues de aplastar el botón", "Error Botón", MessageBoxButtons.OK);
            }
        } // Boton Léxico

        private void metroButton1_Click(object sender, EventArgs e) //Botón Sintaxis
        {
            bool sintaxis = true;
            EjecutarSintaxis(sintaxis);
        }

        private void btnListaPolish_Click(object sender, EventArgs e)
        {
            bool semantica = false;
            listaPolish.Clear();
            verListaPolish = null;
            mesSem = 1;
            EjecutarSemantica(semantica);
            foreach (NodoPolish item in listaPolish)
            {
                verListaPolish += item.lexema + "   " + item.flag + "\n";
            }
            MessageBox.Show(this, verListaPolish, "Lista de Polish");
        } //Botón Lista Polish

        private void btnSemantica_Click(object sender, EventArgs e) //Botón Semántica
        {
            bool semantica = false;
            EjecutarSemantica(semantica);
        }

        private void btnCompilar_Click(object sender, EventArgs e)
        {
            bool semantica = false;
            asm.code = null;
            listaPolish.Clear();
            asm.listaStrings.Clear();
            verListaPolish = null;
            mesSem = 1;
            EjecutarSemantica(semantica);
            asm.listaPolish = listaPolish;
            asm.listaVariables = listaDeVariables;
            asm.Compilar();
            MessageBox.Show(this, "Archivo .asm genereado", "Proceso Terminado", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }  //Botón Compilar
        #endregion

        #region Léxico
        public void Guardar(string lexema, int token, int renglon)
        {
            Nodo nodo = new Nodo(lexema, token, renglon);
            if (cabeza == null)
            {
                cabeza = nodo;
                p = nodo;
                listaDeNodos.Add(cabeza);
            }
            else
            {
                p.sig = nodo;
                p = nodo;
                listaDeNodos.Add(nodo);
            }
        }

        #region Matriz de transición
        private void LlenarMatrizT()
        {
            matrizT[0, 0] = 1;
            matrizT[0, 1] = 2;
            matrizT[0, 2] = 113;
            matrizT[0, 3] = 114;
            matrizT[0, 4] = 115;
            matrizT[0, 5] = 5;
            matrizT[0, 6] = 16;
            matrizT[0, 7] = 103;
            matrizT[0, 8] = 104;
            matrizT[0, 9] = 105;
            matrizT[0, 10] = 9;
            matrizT[0, 11] = 107;
            matrizT[0, 12] = 108;
            matrizT[0, 13] = 109;
            matrizT[0, 14] = 110;
            matrizT[0, 15] = 111;
            matrizT[0, 16] = 112;
            matrizT[0, 17] = 117;
            matrizT[0, 18] = 10;
            matrizT[0, 19] = 11;
            matrizT[0, 20] = 12;
            matrizT[0, 21] = 13;
            matrizT[0, 22] = 14;
            matrizT[0, 23] = 15;
            matrizT[0, 24] = 0;
            matrizT[0, 25] = 0;
            matrizT[0, 26] = 0;
            matrizT[0, 27] = 0;
            matrizT[0, 28] = 0;
            matrizT[0, 29] = 0;

            for (int i = 0; i <= 29; i++) //Renglón 1
            {
                matrizT[1, i] = 100;
                matrizT[1, 0] = 1;
                matrizT[1, 1] = 1;
            }

            for (int i = 0; i <= 29; i++) //Renglón 2
            {
                matrizT[2, i] = 101;
                matrizT[2, 1] = 2;
                matrizT[2, 7] = 3;
            }

            for (int i = 0; i <= 29; i++) //Renglón 3
            {
                matrizT[3, i] = 500;
                matrizT[3, 1] = 4;
            }

            for (int i = 0; i <= 29; i++) //Renglón 4
            {
                matrizT[4, i] = 102;
                matrizT[4, 1] = 4;
            }

            for (int i = 0; i <= 29; i++) //Renglón 5
            {
                matrizT[5, i] = 116;
                matrizT[5, 4] = 7;
                matrizT[5, 5] = 6;
            }

            for (int i = 0; i <= 29; i++) //Renglón 6
            {
                matrizT[6, i] = 6;
                matrizT[6, 24] = 0;
            }

            for (int i = 0; i <= 29; i++) //Renglón 7
            {
                matrizT[7, i] = 7;
                matrizT[7, 4] = 8;
                matrizT[7, 28] = 506;
            }

            for (int i = 0; i <= 29; i++) //Renglón 8
            {
                matrizT[8, i] = 7;
                matrizT[8, 5] = 0;
                matrizT[8, 28] = 506;
            }

            for (int i = 0; i <= 29; i++) //Renglón 9
            {
                matrizT[9, i] = 504;
                matrizT[9, 6] = 124;
            }

            for (int i = 0; i <= 29; i++) //Renglón 10
            {
                matrizT[10, i] = 121;
                matrizT[10, 6] = 123;
            }

            for (int i = 0; i <= 29; i++) //Renglón 11
            {
                matrizT[11, i] = 120;
                matrizT[11, 6] = 122;
            }

            for (int i = 0; i <= 29; i++) //Renglón 12
            {
                matrizT[12, i] = 128;
                matrizT[12, 6] = 119;
            }

            for (int i = 0; i <= 29; i++) //Renglón 13
            {
                matrizT[13, i] = 13;
                matrizT[13, 21] = 125;
                matrizT[13, 28] = 501;
            }

            for (int i = 0; i <= 29; i++) //Renglón 14
            {
                matrizT[14, i] = 502;
                matrizT[14, 22] = 126;
            }

            for (int i = 0; i <= 29; i++) //Renglón 15
            {
                matrizT[15, i] = 503;
                matrizT[15, 23] = 127;
            }

            for (int i = 0; i <= 29; i++) //Renglón 16
            {
                matrizT[16, i] = 505;
                matrizT[16, 6] = 118;
            }

        }
        #endregion

        public void Imprimir()
        {
            try
            {
                tablaTokens.Rows.Clear();
                foreach (Nodo node in listaDeNodos)
                {
                    tablaTokens.Rows.Add(node.lexema, node.token, node.renglon);
                }
                txtCodigo.Hide();
                tablaTokens.Show();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Error en la tabla", "Error Tabla", MessageBoxButtons.OK);
            }
        }

        #region Palabras reservadas
        private void LlenasPalabrasreservadas()
        {
            palabrasReservadas[0] = "if";
            palabrasReservadas[1] = "else";
            palabrasReservadas[2] = "for";
            palabrasReservadas[3] = "break";
            palabrasReservadas[4] = "continue";
            palabrasReservadas[5] = "true";
            palabrasReservadas[6] = "false";
            palabrasReservadas[7] = "print";
            palabrasReservadas[8] = "go";
            palabrasReservadas[9] = "package";
            palabrasReservadas[10] = "main";
            palabrasReservadas[11] = "func";
            palabrasReservadas[12] = "var";
            palabrasReservadas[13] = "const";
            palabrasReservadas[14] = "type";
            palabrasReservadas[15] = "int";
            palabrasReservadas[16] = "string";
            palabrasReservadas[17] = "bool";
            palabrasReservadas[18] = "float";
            palabrasReservadas[19] = "while";
            palabrasReservadas[20] = "char";
            palabrasReservadas[21] = "scan";

        }

        public void BuscarPalabraReservada(string palabra)
        {
            palabra.ToLower();
            switch (palabra)
            {
                case "if":
                    valorMT = 200;
                    break;

                case "else":
                    valorMT = 201;
                    break;

                case "for":
                    valorMT = 202;
                    break;

                case "break":
                    valorMT = 203;
                    break;

                case "continue":
                    valorMT = 204;
                    break;

                case "true":
                    valorMT = 205;
                    break;

                case "false":
                    valorMT = 206;
                    break;

                case "print":
                    valorMT = 207;
                    break;

                case "go":
                    valorMT = 208;
                    break;

                case "package":
                    valorMT = 209;
                    break;

                case "main":
                    valorMT = 210;
                    break;

                case "func":
                    valorMT = 211;
                    break;

                case "var":
                    valorMT = 212;
                    break;

                case "const":
                    valorMT = 213;
                    break;

                case "type":
                    valorMT = 214;
                    break;

                case "int":
                    valorMT = 215;
                    break;

                case "string":
                    valorMT = 216;
                    break;

                case "bool":
                    valorMT = 217;
                    break;

                case "float":
                    valorMT = 218;
                    break;

                case "while":
                    valorMT = 219;
                    break;

                case "char":
                    valorMT = 220;
                    break;

                case "scan":
                    valorMT = 221;
                    break;
            }


        }
        #endregion

        public void ValidarLéxico()
        {
            estado = 0;
            i = 0;
            renglon = 1;
            cadena = "";
            bool AvanzarToken = false;
            while (i < code.Length)
            {
                caracter = code[i];
                if (i == (code.Length - 1) && caracter != '}')
                {
                    eof = true;
                    cadenaStringAux += cadena + caracter;
                    cadena = "";
                }
                if ((estado == 0 && caracter != ' ' && caracter != '\n' && caracter != '\r' && caracter != '\t') || estado > 0 || (caracter == ';' && cadena.Length == 0))
                {
                    cadena += caracter;
                }
                VerificaCodigo(caracter);


                if (valorMT < 100) //Estado de transición
                {
                    estado = valorMT;
                    i++;

                }
                else if (valorMT >= 100 && valorMT < 500) //Estados finales
                {
                    char ultimo = cadena[cadena.Length - 1];
                    char buscarNot = cadena[0];

                    if (((valorMT == 101 || valorMT == 102) && Char.IsLetter(ultimo)) || ultimo == ' ' || cadena[cadena.Length - 1] == '\r' || ultimo == '\n' || (cadena.Length == 2 && buscarNot == '!' && ultimo != '=') || cadena[cadena.Length - 1] == '\t' || ((valorMT == 100 || valorMT == 101 || valorMT >= 200 || valorMT == 102) && !(Char.IsLetter(ultimo) || Char.IsDigit(ultimo))) || (!(valorMT == 100 || valorMT == 101 || valorMT >= 200 || valorMT == 102) && (Char.IsLetter(ultimo) || Char.IsDigit(ultimo))) || (cadena.Length > 1 && (caracter == '<' || caracter == '>') && (ultimo != '=')))
                    {
                        cadena = cadena.Remove(cadena.Length - 1);
                        AvanzarToken = true;

                    }
                    if (valorMT == 100)
                    {
                        BuscarPalabraReservada(cadena);

                    }
                    Guardar(cadena, valorMT, renglon);
                    estado = 0;
                    if (valorMT == 125 || valorMT == 101 || valorMT == 102)
                    {
                        if (valorMT == 101 || valorMT == 102)
                            AddStringToListForASM(cadena, asm.listaNumeros);
                        else
                            AddStringToListForASM(cadena.Trim('"'), asm.listaStrings);

                    }

                    if (!AvanzarToken)
                    {
                        i++;
                    }
                    AvanzarToken = false;


                    valorMT = 0;
                    cadena = "";

                }
                else if (valorMT >= 500)
                {
                    //Errores
                    switch (valorMT)
                    {
                        case 500:
                            mensaje = "Número decimal incompleto";
                            titulo = "Número decimal";
                            break;

                        case 501:
                            mensaje = "Se espera fin de cadena";
                            titulo = "Comentario inváildo";
                            renglon = listaDeNodos.Last().renglon;
                            cadena = cadenaStringAux;
                            break;

                        case 502:
                            mensaje = "Se espera &";
                            titulo = "&&";
                            break;

                        case 503:
                            mensaje = "Se espera |";
                            titulo = "||";
                            break;

                        case 504:
                            mensaje = "Se espera =";
                            titulo = ":=";
                            break;

                        case 505:
                            mensaje = "Símbolo inválido. Se espera ':' o '='";
                            titulo = "==";
                            break;

                        case 506:
                            mensaje = "Fin de comentario inválido";
                            renglon = listaDeNodos.Last().renglon;
                            cadena = cadenaStringAux;
                            titulo = "Comentarios inválido";
                            break;

                    }
                    break;

                }
            }
        }

        public void VerificaCodigo(char caracter)
        {
            #region Posicionar columna
            if (Char.IsLetter(caracter))
            {
                columna = 0;
            }
            else if (Char.IsDigit(caracter))
            {
                columna = 1;
            }
            else
            {
                switch (caracter)
                {
                    case '+':
                        columna = 2;
                        break;

                    case '-':
                        columna = 3;
                        break;

                    case '*':
                        columna = 4;
                        break;

                    case '/':
                        columna = 5;
                        break;

                    case '=':
                        columna = 6;
                        break;

                    case '.':
                        columna = 7;
                        break;

                    case ';':
                        columna = 8;
                        break;

                    case ',':
                        columna = 9;
                        break;

                    case ':':
                        columna = 10;
                        break;

                    case '(':
                        columna = 11;
                        break;

                    case ')':
                        columna = 12;
                        break;

                    case '{':
                        columna = 13;
                        break;

                    case '}':
                        columna = 14;
                        break;

                    case '[':
                        columna = 15;
                        break;

                    case ']':
                        columna = 16;
                        break;

                    case '%':
                        columna = 17;
                        break;

                    case '>':
                        columna = 18;
                        break;

                    case '<':
                        columna = 19;
                        break;

                    case '!':
                        columna = 20;
                        break;

                    case '"':
                        columna = 21;
                        break;

                    case '&':
                        columna = 22;
                        break;

                    case '|':
                        columna = 23;
                        break;

                    case '\n':
                        columna = 24;
                        renglon++;
                        cadenaStringAux += cadena;
                        cadena = "";
                        break;

                    case ' ':
                        columna = 25;
                        break;

                    // case 'eol': columna = 26;
                    //     break;

                    case '\t':
                        columna = 27;
                        break;

                    //case 26 eof: columna = 28;
                    //     break;

                    default:
                        columna = 29;
                        break;

                }
            }
            #endregion

            if (eof)
                columna = 28;
            valorMT = matrizT[estado, columna];
            eof = false;


        }
        #endregion

        #region Semántica
        public bool BuscarVariableEnLista(string variable)
        {
            bool flag = false;
            for (int h = 0; h < listaDeVariables.Count; h++)
            {
                if (variable == listaDeVariables[h].lexema)
                {
                    tokenVariableDefinida = listaDeVariables[h].token;
                    lexemaMensaje = listaDeVariables[h].lexema;
                    flag = true;
                    tipoVariable = listaDeVariables[h].token;
                    break;
                }
                else
                {
                    flag = false;
                }
            }
            for (int i = 0; i < pilaVariablesSemantica.Count; i++)
            {
                if (variable == pilaVariablesSemantica[i] || flag == true)
                {
                    flag = true;
                    break;
                }
                else
                {
                    flag = false;
                }
            }
            return flag;
        }

        public int ConvertirTokenSemantica(int token)
        {
            switch (token)
            {
                case 1:
                    token = 1;
                    break;

                case 2:
                    token = 2;
                    break;

                case 3:
                    token = 3;
                    break;

                case 4:
                    token = 4;
                    break;

                case 101:
                    token = 1;
                    break;

                case 102:
                    token = 2;
                    break;

                case 107:
                    token = 16;
                    break;

                case 108:
                    token = 17;
                    break;

                case 113:
                    token = 5;
                    break;

                case 114:
                    token = 6;
                    break;

                case 115:
                    token = 7;
                    break;

                case 116:
                    token = 8;
                    break;

                case 117:
                    token = 22;
                    break;

                case 118:
                    token = 20;
                    break;

                case 119:
                    token = 21;
                    break;

                case 120:
                    token = 9;
                    break;

                case 121:
                    token = 10;
                    break;

                case 122:
                    token = 11;
                    break;

                case 123:
                    token = 12;
                    break;

                case 124:
                    token = 13;
                    break;

                case 125:
                    token = 3;
                    break;

                case 126:
                    token = 14;
                    break;

                case 127:
                    token = 15;
                    break;

                case 205:
                    token = 4;
                    break;

                case 206:
                    token = 4;
                    break;

                case 207:
                    token = 207;
                    break;

                case 221:
                    token = 221;
                    break;

                default:
                    tituloSemantica = "Operador u operando inválido";
                    mensajeSemantica = "El operador " + token.ToString() + " no es válido en renglón ";
                    MensajeSemantica(token.ToString());
                    break;
            }
            return token;
        }

        public void EjecutarSistemaDeTipos()
        {
            for (int i = 0; i < listaSemanticaOperaciones.Count; i++)
            {
                if (listaSemanticaOperaciones[i].token > 4)
                {
                    int op1;
                    int op2 = pilaOperandos.Pop().token;
                    if (listaSemanticaOperaciones[i].token == 18 || listaSemanticaOperaciones[i].token == 19)
                    {
                        Nodo nod = new Nodo(null, SistemaDeTipos(listaSemanticaOperaciones[i].token, op2, 0), renglon);
                        pilaOperandos.Push(nod);
                    }
                    else
                    {
                        op1 = pilaOperandos.Pop().token;
                        Nodo nod = new Nodo(null, SistemaDeTipos(listaSemanticaOperaciones[i].token, op1, op2), renglon);
                        pilaOperandos.Push(nod);
                    }
                }
                else
                {
                    pilaOperandos.Push(listaSemanticaOperaciones[i]);
                }
            }
        }

        public void GuardarVariables(string lexema, int token)
        {
            Nodo nodo = new Nodo(lexema, tokenTipo, listaDeNodos[contador].renglon);
            if (cabezaSemantica == null)
            {
                cabezaSemantica = nodo;
                pSemantica = nodo;
                listaDeVariables.Add(cabezaSemantica);
            }
            else
            {
                pSemantica.sig = nodo;
                pSemantica = nodo;
                listaDeVariables.Add(nodo);
            }
        }

        public void MensajeSemantica(string lexema)
        {
            try
            {
                int renglonMensaje = renglon;
                string lexemaMensaje = lexema;
                if (contador > 0)
                {
                    if ((renglon > listaDeNodos[contador - 1].renglon && listaDeNodos[contador - 1].token != 104 && listaDeNodos[contador - 1].token != 107 && listaDeNodos[contador - 1].token != 109 && listaDeNodos[contador - 1].token != 110) && contador > 1)
                    {
                        renglonMensaje = listaDeNodos[contador - 1].renglon;
                        lexemaMensaje = listaDeNodos[contador - 1].lexema;
                    }

                }


                if (msg < 1)
                {
                    MessageBox.Show(this, mensajeSemantica + renglonMensaje + " en: " + lexemaMensaje, tituloSemantica, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                msg++;

            }
            catch (Exception)
            {

                contador = 1;
            }
        }

        public void Postfijo()
        {
            pilaOperadores.Clear();
            pilaOperandos.Clear();
            listaSemanticaOperaciones.Clear();
            for (int i = 0; i < listaTemporalOperadores.Count; i++) // Postfijo
            {
                listaTemporalOperadores[i].token = ConvertirTokenSemantica(listaTemporalOperadores[i].token);
                if (listaTemporalOperadores[i].token == 16) // (
                {
                    pilaOperadores.Push(listaTemporalOperadores[i]);
                }
                else if (listaTemporalOperadores[i].token == 17)
                {
                    do
                    {
                        if (pilaOperadores.Peek().token != 16)
                        {
                            listaSemanticaOperaciones.Add(pilaOperadores.Pop());
                        }
                    } while (pilaOperadores.Peek().token != 16);

                    pilaOperadores.Pop();
                }
                else if (listaTemporalOperadores[i].token <= 4) // Operando
                {
                    listaSemanticaOperaciones.Add(listaTemporalOperadores[i]);
                }
                else if (pilaOperadores.Count == 0) // Vacia
                {
                    pilaOperadores.Push(listaTemporalOperadores[i]);
                }
                else if (listaTemporalOperadores[i].token == 207)
                {
                    pilaOperadores.Push(listaTemporalOperadores[i]);
                }
                else if (listaTemporalOperadores[i].token == 221)
                {
                    pilaOperadores.Push(listaTemporalOperadores[i]);
                }
                else  // Operador
                {
                    bool prioridad = Prioridades(listaTemporalOperadores[i].token, pilaOperadores.Peek().token);
                    if (prioridad == false)
                    {
                        pilaOperadores.Push(listaTemporalOperadores[i]);
                    }
                    else
                    {
                        listaSemanticaOperaciones.Add(pilaOperadores.Pop());
                        pilaOperadores.Push(listaTemporalOperadores[i]);
                    }
                }
            }
            while (pilaOperadores.Count > 0)
            {
                if (pilaOperadores.Peek().token != 16)
                {
                    listaSemanticaOperaciones.Add(pilaOperadores.Pop());
                }
                else
                {
                    pilaOperadores.Pop();
                }
            }
            listaTemporalOperadores.Clear();
        }

        public bool Prioridades(int operadorEntrante, int operadorPila) // true - pop; false - push
        {
            bool prioridad = false;
            if (operadorEntrante == 18 || operadorPila == 19) // @, !
            {
                prioridad = false;
            }
            else if (operadorEntrante == 15 || operadorEntrante == 14) // &&  ||
            {
                if (operadorPila == 19 || operadorPila == 16)
                {
                    prioridad = false;
                }
                else
                {
                    prioridad = true;
                }
            }
            else if (operadorEntrante == 9 || operadorEntrante == 10 || operadorEntrante == 11 || operadorEntrante == 12 || operadorEntrante == 20 || operadorEntrante == 21) // <, >, <=, >=, :=, !=
            {
                if (operadorPila == 15 || operadorPila == 14 || operadorPila == 16) // &&, ||, (
                {
                    prioridad = false;
                }
                else
                {
                    prioridad = true;
                }
            }
            else if (operadorEntrante == 7 || operadorEntrante == 8) // *, /
            {
                if (operadorPila == 18 || operadorPila == 7 || operadorPila == 8) // - unitario
                {
                    prioridad = true;
                }
                else
                {
                    prioridad = false;
                }
            }
            else if (operadorEntrante == 5 || operadorEntrante == 6) // +, -
            {
                if (operadorPila == 8 || operadorPila == 7 || operadorPila == 18 || operadorPila == 5 || operadorPila == 6) // *, /, - unitario, +, -
                {
                    prioridad = true;
                }
                else
                {
                    prioridad = false;
                }
            }
            return prioridad;
        }

        public void SemanticaToPolish()
        {
            foreach (Nodo item in listaSemanticaOperaciones)
            {
                NodoPolish node = new NodoPolish(item.lexema, flagPolish, item.token);
                listaPolish.Add(node);
                if (flagPolish != null)
                    flagPolish = null;
            }
            listaSemanticaOperaciones.Clear();
            listaTemporalOperadores.Clear();
        }

        private int SistemaDeTipos(int operador, int op1, int op2)
        {
            int resultado = 0;
            switch (operador)
            {
                case 5: // +
                    switch (op1)
                    {
                        case 1: // int
                            switch (op2)
                            {
                                case 1:
                                    resultado = 1;
                                    break;

                                case 2:
                                    resultado = 2;
                                    break;

                                default:
                                    tituloSemantica = "Variable fuera de contexto";
                                    mensajeSemantica = "Operando inválido en renglón ";
                                    break;
                            }
                            break;

                        case 2: // float
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 2;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";

                            }
                            break;

                        case 3: // string
                            if (op2 == 3)
                            {
                                resultado = 3;
                            }
                            else
                            {

                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";

                            }
                            break;

                        default:
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                            break;
                    }
                    break;

                case 6: // -
                    switch (op1)
                    {
                        case 1: // int
                            switch (op2)
                            {
                                case 1:
                                    resultado = 1;
                                    break;

                                case 2:
                                    resultado = 2;
                                    break;

                                default:
                                    tituloSemantica = "Variable fuera de contexto";
                                    mensajeSemantica = "Operando inválido en renglón ";
                                    break;
                            }
                            break;

                        case 2: // float
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 2;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";

                            }
                            break;

                        default:
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido " + op2 + " en renglón ";
                            break;
                    }
                    break;

                case 7: // *
                    switch (op1)
                    {
                        case 1: // int
                            switch (op2)
                            {
                                case 1:
                                    resultado = 1;
                                    break;

                                case 2:
                                    resultado = 2;
                                    break;

                                default:
                                    tituloSemantica = "Variable fuera de contexto";
                                    mensajeSemantica = "Operando inválido en renglón ";
                                    break;
                            }
                            break;

                        case 2: // float
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 2;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";

                            }
                            break;

                        default:
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido " + op2 + " en renglón ";
                            break;
                    }
                    break;

                case 8: // /
                    if (op1 == 1 || op1 == 2)
                    {
                        if (op2 == 1 || op2 == 2)
                        {
                            resultado = 2;
                        }
                        else
                        {
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                        }
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 22: // %
                    if (op1 == 1 || op1 == 2)
                    {
                        if (op2 == 1 || op2 == 2)
                        {
                            resultado = 1;
                        }
                        else
                        {
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                        }
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 118: // ==
                    switch (op1)
                    {
                        case 1:
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 2:
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 3:
                            if (op2 == 3)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 4: // bool
                            if (op2 == 4)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;
                        default:
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                            break;
                    }
                    break;

                case 9: // <
                    if (op1 == 1 || op1 == 2)
                    {
                        if (op2 == 1 || op2 == 2)
                        {
                            resultado = 4;
                        }
                        else
                        {
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                        }
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 10: // >
                    if (op1 == 1 || op1 == 2)
                    {
                        if (op2 == 1 || op2 == 2)
                        {
                            resultado = 4;
                        }
                        else
                        {
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                        }
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 11: // <=
                    if (op1 == 1 || op1 == 2)
                    {
                        if (op2 == 1 || op2 == 2)
                        {
                            resultado = 4;
                        }
                        else
                        {
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                        }
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 12: // >=
                    if (op1 == 1 || op1 == 2)
                    {
                        if (op2 == 1 || op2 == 2)
                        {
                            resultado = 4;
                        }
                        else
                        {
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                        }
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 13: // :=
                    switch (op1)
                    {
                        case 1:
                            if (op2 != 1)
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido. No se puede asignar ese tipo de dato a un entero. Renglón ";
                            }
                            else
                            {
                                resultado = 1;
                            }
                            break;

                        case 2:
                            if (op2 != 1 && op2 != 2)
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido. No se puede asignar ese tipo de dato a un float. Renglón ";
                            }
                            else
                            {
                                resultado = 2;
                            }
                            break;

                        case 3:
                            if (op2 != 3)
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido. No se puede asignar ese tipo de dato a un string. Renglón ";
                            }
                            else
                            {
                                resultado = 3;
                            }
                            break;

                        case 4: // bool
                            if (op2 != 4)
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido. No se puede asignar ese tipo de dato a un booleano. Renglón ";
                            }
                            else
                            {
                                resultado = 4;
                            }
                            break;
                        default:
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                            break;
                    }
                    break;

                case 14: // &&
                    if (op1 == 4 && op2 == 4)
                    {
                        resultado = 4;
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 15: // ||
                    if (op1 == 4)
                    {
                        if (op2 == 4)
                        {
                            resultado = 4;
                        }
                        else
                        {
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                        }
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 18: // @
                    if (op1 == 4)
                    {
                        resultado = 4;
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 19: // !
                    if (op1 == 4)
                    {
                        resultado = 4;
                    }
                    else
                    {
                        tituloSemantica = "Variable fuera de contexto";
                        mensajeSemantica = "Operando inválido en renglón ";
                    }
                    break;

                case 20: // ==
                    switch (op1)
                    {
                        case 1:
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 2:
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 3:
                            if (op2 == 3)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 4: // bool
                            if (op2 == 4)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;
                        default:
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                            break;
                    }
                    break;

                case 21: // !=
                    switch (op1)
                    {
                        case 1:
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 2:
                            if (op2 == 1 || op2 == 2)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 3:
                            if (op2 == 3)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;

                        case 4: // bool
                            if (op2 == 4)
                            {
                                resultado = 4;
                            }
                            else
                            {
                                tituloSemantica = "Variable fuera de contexto";
                                mensajeSemantica = "Operando inválido en renglón ";
                            }
                            break;
                        default:
                            tituloSemantica = "Variable fuera de contexto";
                            mensajeSemantica = "Operando inválido en renglón ";
                            break;
                    }
                    break;
            }
            if (resultado == 0 && operador != 13)
            {
                MessageBox.Show(this, "Variable o valor fuera de contexto", tituloSemantica);
            }
            return resultado;

        }

        #region Tokens Semántica
        //int -    1
        //float -  2
        //string - 3
        //bool -   4
        //  + ---  5
        //  - ---  6
        //  * ---  7
        //  / ---  8
        //  < ---  9
        //  > ---  10
        // <= ---  11
        // >= ---  12
        // := ---  13
        // && ---  14
        // || ---  15
        // (  ---  16
        // )  ---  17
        // @  ---  18
        // !  ---  19
        // == ---  20
        // != ---  21
        // %  ---  22 
        #endregion
        #endregion

        #region Sintaxis
        public void Avanzar()
        {
            try
            {
                contador++;
                token = listaDeNodos[contador].token;
                renglon = listaDeNodos[contador].renglon;
            }
            catch (Exception)
            {
                tituloSintaxis = "Error de terminación";
                mensajeSintaxis = "Falta '}' al final de código";
                if (msg < 1)
                    MessageBox.Show(this, mensajeSintaxis, tituloSintaxis, MessageBoxButtons.OK, MessageBoxIcon.Error);
                msg++;
            }
        }

        #region Block, Statements
        public void Block() // Todo el código; Statements
        {
            switch (token)
            {
                case 200: // if
                    Avanzar();
                    IfStmt();
                    break;

                case 202: // for
                    Avanzar();
                    ForStmt();
                    break;

                case 221: // read
                    GuardarPolish("scan", token, renglon);
                    Avanzar();
                    ReadStmt();
                    break;

                case 207: // print
                    GuardarPolish("print", token, renglon);
                    Avanzar();
                    PrintStmt();
                    break;

                case 100: // asignación
                    if (BuscarVariableEnLista(listaDeNodos[contador].lexema) == true)
                    {
                        GuardarPolish(listaDeNodos[contador].lexema, tipoVariable, renglon);
                        Avanzar();
                        AsignacionStmt();
                    }
                    else
                    {
                        tituloSemantica = "Variable No Declarada 558";
                        mensajeSemantica = "Falta declarar la variable" + listaDeNodos[contador].lexema + " en renglón ";
                        MensajeSemantica(listaDeNodos[contador].lexema);
                    }
                    break;

                default:
                    if (token != 110)
                    {
                        tituloSintaxis = "Error";
                        mensajeSintaxis = "Sentencia inválida en renglón 519";
                        MensajeSintaxis();
                    }
                    if (listaDeNodos[contador].sig != null)
                    {
                        Avanzar();
                    }
                    Avanzar();
                    break;
            }
        }

        public void IfStmt() //Código IF 
        {
            contadorIf++;
            string A = "A" + contadorIf.ToString();
            string B = "B" + contadorIf.ToString();
            try
            {
                if (token == 107) // (
                {
                    GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                    Avanzar();
                    Expresion_Logica(); //Expresion_Lógica
                    if (token == 108)// )
                    {
                        GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                        Postfijo();
                        EjecutarSistemaDeTipos();
                        SemanticaToPolish();
                        Avanzar();
                        NodoPolish nA = new NodoPolish("Br-F " + A, flagPolish, token);
                        listaPolish.Add(nA);
                        if (flagPolish != null)
                            flagPolish = null;
                        if (token == 109) // {
                        {
                            Avanzar();
                            while (token != 110)
                            {
                                if (msg > 0)
                                {
                                    contador = listaDeNodos.Count() - 1;
                                }
                                if (listaDeNodos[contador].sig == null)
                                {
                                    break;
                                }
                                else if (token == 100 || token == 200 || token == 202 || token == 207 || token == 221)
                                {
                                    while ((token == 100 || token == 200 || token == 202 || token == 207 || token == 221) && msg == 0)
                                    {
                                        Block();// Cualquier Otro Contenido 
                                    }
                                }
                                else
                                    Avanzar();
                            }
                            NodoPolish nB = new NodoPolish("Br-I " + B, flagPolish, token);
                            listaPolish.Add(nB);
                            if (flagPolish != null)
                                flagPolish = null;
                            if (token == 110) // }
                            {
                                Avanzar();
                                if (flagPolish == null)
                                {
                                    flagPolish = A;
                                }
                                else
                                {
                                    flagPolish += " , " + A;
                                }
                                if (token == 201) //else
                                {
                                    Avanzar();
                                    if (token == 109) // {
                                    {
                                        Avanzar();
                                        while (token != 110)
                                        {
                                            if (msg > 0)
                                            {
                                                contador = listaDeNodos.Count() - 1;
                                            }
                                            if (listaDeNodos[contador].sig == null)
                                            {
                                                break;
                                            }
                                            else if (token == 100 || token == 200 || token == 202 || token == 207 || token == 221)
                                            {
                                                while ((token == 100 || token == 200 || token == 202 || token == 207 || token == 221) && msg == 0)
                                                {
                                                    Block();// Cualquier Otro Contenido 
                                                }
                                            }
                                            else
                                                Avanzar();
                                        }
                                        if (token == 110) // }
                                        {
                                            Avanzar();
                                        }
                                    }
                                }
                                if (flagPolish == null)
                                {
                                    flagPolish = B;
                                }
                                else
                                {
                                    flagPolish += " , " + B;
                                }
                            }
                            else
                            {
                                tituloSintaxis = "Error de IF 523";
                                mensajeSintaxis = "Se espera terminar con '}' en renglón ";
                                MensajeSintaxis();
                            }
                        }
                        else
                        {
                            tituloSintaxis = "Error de inicio de IF 522";
                            mensajeSintaxis = "Se espera '{' en renglón ";
                            MensajeSintaxis();
                        }
                    }
                    else
                    {
                        tituloSintaxis = "Error de inicio de IF 521";
                        mensajeSintaxis = "Se espera ')' en renglón ";
                        MensajeSintaxis();
                    }
                }
                else
                {
                    tituloSintaxis = "Error de inicio de IF 520";
                    mensajeSintaxis = "Se espera iniciar con '(' en renglón ";
                    MensajeSintaxis();
                }
            }
            catch (Exception)
            {

                tituloSintaxis = "Error en IF ";
                mensajeSintaxis = "Error fatal en IF";
                MensajeSintaxis();
            }
        }

        public void ForStmt() //Código FOR
        {
            contadorFor++;
            string C = "C" + contadorFor.ToString();
            string D = "D" + contadorFor.ToString();
            List<NodoPolish> listaIncremento = new List<NodoPolish>();
            try
            {
                if (token == 107) // (
                {
                    Avanzar();
                    if (token == 100) // x
                    {
                        string varAsignacion = listaDeNodos[contador].lexema;
                        if (BuscarVariableEnLista(varAsignacion) == false)
                        {
                            GuardarVariables(varAsignacion, 1);
                            GuardarPolish(varAsignacion, 1, renglon);
                            Avanzar();
                        }
                        else
                        {
                            tituloSemantica = "Variable ya declarada";
                            mensajeSemantica = "La variable ya fue declarada ";
                            MensajeSemantica(varAsignacion);
                        }
                        if (token == 124) // :=
                        {
                            GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                            Avanzar();
                            if (token == 101) // numero entero
                            {
                                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                                Postfijo();
                                EjecutarSistemaDeTipos();
                                SemanticaToPolish();
                                Avanzar();
                                if (token == 104) // ;
                                {
                                    Avanzar();
                                    if (flagPolish == null)
                                    {
                                        flagPolish = D;
                                    }
                                    else
                                    {
                                        flagPolish += " , " + D;
                                    }
                                    Expresion_Logica(); // <expresion_lógica>
                                    Postfijo();
                                    SemanticaToPolish();
                                    NodoPolish nC = new NodoPolish("Br-F " + C, flagPolish, token);
                                    listaPolish.Add(nC);
                                    if (token == 104) // ;
                                    {
                                        Avanzar();
                                        if (token == 100) // x
                                        {
                                            if (listaDeNodos[contador].lexema == varAsignacion)
                                            {
                                                GuardarPolish(listaDeNodos[contador].lexema, 1, renglon);
                                                Avanzar();
                                            }
                                            else
                                            {
                                                tituloSintaxis = "Error de parámetros";
                                                mensajeSintaxis = "Contador de FOR erroneo en " + listaDeNodos[contador].lexema + " en renglón ";
                                                MensajeSintaxis();
                                            }
                                            if (token == 124) // :=
                                            {
                                                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);

                                                Avanzar();
                                                if (token == 100) //x
                                                {
                                                    if (listaDeNodos[contador].lexema == varAsignacion)
                                                    {
                                                        GuardarPolish(listaDeNodos[contador].lexema, 1, renglon);
                                                        Avanzar();
                                                    }
                                                    else
                                                    {
                                                        tituloSintaxis = "Error de parámetros";
                                                        mensajeSintaxis = "Contador de FOR erroneo en " + listaDeNodos[contador].lexema + " en renglón ";
                                                        MensajeSintaxis();
                                                    }
                                                    if (token == 113 || token == 114) // + ó -
                                                    {
                                                        GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                                                        Avanzar();
                                                        if (token == 101) // entero
                                                        {
                                                            GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                                                            Postfijo();
                                                            EjecutarSistemaDeTipos();
                                                            foreach (Nodo item in listaSemanticaOperaciones)
                                                            {
                                                                NodoPolish node = new NodoPolish(item.lexema, flagPolish, token);
                                                                listaIncremento.Add(node);
                                                                if (flagPolish != null)
                                                                    flagPolish = null;
                                                            }
                                                            listaSemanticaOperaciones.Clear();
                                                            listaTemporalOperadores.Clear();
                                                            Avanzar();
                                                            if (token == 108) // )
                                                            {
                                                                Avanzar();
                                                                if (token == 109) // {
                                                                {
                                                                    Avanzar();
                                                                    if (token == 203) // break
                                                                    {
                                                                        Avanzar();
                                                                        if (token == 104) // ;
                                                                        {
                                                                            Avanzar();
                                                                        }
                                                                        else
                                                                        {
                                                                            tituloSintaxis = "Error de fin de linea en BREAK de FOR";
                                                                            mensajeSintaxis = "Se espera ';' en renglón ";
                                                                            MensajeSintaxis();
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if (token == 204) // continue
                                                                        {
                                                                            Avanzar();
                                                                            if (token == 104) // ;
                                                                            {
                                                                                Avanzar();
                                                                            }
                                                                            else
                                                                            {
                                                                                tituloSintaxis = "Error de fin de linea de CONTINUE de FOR";
                                                                                mensajeSintaxis = "Se espera ';' en renglón ";
                                                                                MensajeSintaxis();
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            while (token != 110)
                                                                            {
                                                                                if (msg > 0)
                                                                                {
                                                                                    contador = listaDeNodos.Count() - 1;
                                                                                }
                                                                                if (listaDeNodos[contador].sig == null)
                                                                                {
                                                                                    break;
                                                                                }
                                                                                else if (token == 100 || token == 200 || token == 202 || token == 207 || token == 221)
                                                                                {
                                                                                    while ((token == 100 || token == 200 || token == 202 || token == 207 || token == 221) && msg == 0)
                                                                                    {
                                                                                        Block();// Cualquier Otro Contenido 
                                                                                    }
                                                                                }
                                                                                else
                                                                                    Avanzar();
                                                                                listaIncremento[0].flag = flagPolish;
                                                                                if (flagPolish != null)
                                                                                    flagPolish = null;
                                                                                listaPolish.AddRange(listaIncremento);
                                                                                NodoPolish nD = new NodoPolish("Br-I " + D, flagPolish, token);
                                                                                listaPolish.Add(nD);
                                                                                if (flagPolish != null)
                                                                                    flagPolish = null;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (token == 110) // }
                                                                    {
                                                                        Avanzar();
                                                                        if (flagPolish == null)
                                                                        {
                                                                            flagPolish = C;
                                                                        }
                                                                        else
                                                                        {
                                                                            flagPolish += " , " + C;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        tituloSintaxis = "Error de fin de FOR 537";
                                                                        mensajeSintaxis = "Se espera '}' en renglón ";
                                                                        MensajeSintaxis();
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    tituloSintaxis = "Error de inicio de FOR 536";
                                                                    mensajeSintaxis = "Se debe iniciar con '{' en renglón ";
                                                                    MensajeSintaxis();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                tituloSintaxis = "Error de parámetros de FOR 535";
                                                                mensajeSintaxis = "Se debe terminar con ')' o '-' en renglón ";
                                                                MensajeSintaxis();
                                                            }
                                                        }
                                                        else
                                                        {
                                                            tituloSintaxis = "Error de parámetros de FOR 534";
                                                            mensajeSintaxis = "Se debe usar un número entero detro del tercer parámetro en renglón ";
                                                            MensajeSintaxis();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        tituloSintaxis = "Error de parámetros de FOR 533";
                                                        mensajeSintaxis = "Se debe seguir la igualación del tercer parámetro con '+' o '-' en renglón ";
                                                        MensajeSintaxis();
                                                    }
                                                }
                                                else
                                                {
                                                    tituloSintaxis = "Error de parámetros de FOR 532";
                                                    mensajeSintaxis = "Se debe iniciar la igualación del tercer parámetro con un identificador en renglón ";
                                                    MensajeSintaxis();
                                                }
                                            }
                                            else
                                            {
                                                tituloSintaxis = "Error de parámetros de FOR 531";
                                                mensajeSintaxis = "Se debe igualar el identificador del tercer parámetro usando ':=' en renglón ";
                                                MensajeSintaxis();
                                            }
                                        }
                                        else
                                        {
                                            tituloSintaxis = "Error de parámetros de FOR 530";
                                            mensajeSintaxis = "Se debe iniciar el tercer parámetro con un identificador en renglón ";
                                            MensajeSintaxis();
                                        }
                                    }
                                    else
                                    {
                                        tituloSintaxis = "Error de parámetros de FOR 529";
                                        mensajeSintaxis = "Se debe terminar el segundo parametro con ';' en renglón ";
                                        MensajeSintaxis();
                                    }
                                }
                                else
                                {
                                    tituloSintaxis = "Error de parámetros de FOR 528";
                                    mensajeSintaxis = "Se debe terminar el primer parámetro con ';' en renglón ";
                                    MensajeSintaxis();
                                }
                            }
                            else
                            {
                                tituloSintaxis = "Error de parámetros de FOR 527";
                                mensajeSintaxis = "Se debe igualar el identificador con un número entero en renglón ";
                                MensajeSintaxis();
                            }
                        }
                        else
                        {
                            tituloSintaxis = "Error de parámetros de FOR 526";
                            mensajeSintaxis = "Se debe igualael identificador con un ':=' en renglón ";
                            MensajeSintaxis();
                        }
                    }
                    else
                    {
                        tituloSintaxis = "Error de parámetros de FOR 525";
                        mensajeSintaxis = "Se debe iniciar con un identificador en renglón ";
                        MensajeSintaxis();
                    }
                }
                else
                {
                    tituloSintaxis = "Error de inicio de FOR 524";
                    mensajeSintaxis = "Se espera iniciar con '(' en renglón ";
                    MensajeSintaxis();
                }
            }
            catch (Exception)
            {
                tituloSintaxis = "Error en FOR";
                mensajeSintaxis = "Error fatal en FOR";
                MensajeSintaxis();
            }
        }

        public void ReadStmt() //Código READ
        {
            if (token == 107) // (
            {
                Avanzar();
                if (token == 100) // id
                {
                    if (BuscarVariableEnLista(listaDeNodos[contador].lexema) == true)
                    {
                        GuardarPolish(listaDeNodos[contador].lexema, tipoVariable, renglon);
                        Avanzar();
                    }
                    else
                    {
                        tituloSemantica = "Variable No Declarada 558";
                        mensajeSemantica = "Falta declarar la variable" + listaDeNodos[contador].lexema + " en renglón ";
                        MensajeSemantica(listaDeNodos[contador].lexema);
                    }
                    if (token == 108) // )
                    {
                        Avanzar();
                        if (token == 104) // ;
                        {
                            Avanzar();
                        }
                        else
                        {
                            tituloSintaxis = "Error de fin de linea en lectura 518";
                            mensajeSintaxis = "Se espera ';' en renglón ";
                            MensajeSintaxis();
                        }
                        Postfijo();
                        SemanticaToPolish();
                    }
                    else
                    {
                        tituloSintaxis = "Error de lectura 540";
                        mensajeSintaxis = "Se espera ')' en renglón ";
                        MensajeSintaxis();
                    }
                }
                else
                {
                    tituloSintaxis = "Error de lectura 539";
                    mensajeSintaxis = "Se espera un identificador en renglón ";
                    MensajeSintaxis();
                }
            }
            else
            {
                tituloSintaxis = "Error de lectura 538";
                mensajeSintaxis = "Se espera '(' en renglón ";
                MensajeSintaxis();
            }
        }

        public void PrintStmt() //Código PRINT
        {
            if (token == 107) // (
            {
                Avanzar();
                if (token == 100 || token == 101 || token == 102 || token == 125) // id, numero o cadena
                {
                    if (token == 100)
                    {
                        if (BuscarVariableEnLista(listaDeNodos[contador].lexema) == true)
                        {
                            GuardarPolish(listaDeNodos[contador].lexema, tipoVariable, renglon);
                            Avanzar();
                        }
                        else
                        {
                            tituloSemantica = "Variable No Declarada 558";
                            mensajeSemantica = "Falta declarar la variable" + listaDeNodos[contador].lexema + " en renglón ";
                            MensajeSemantica(listaDeNodos[contador].lexema);
                        }
                    }
                    else
                    {
                        GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                        Avanzar();
                    }
                    if (token == 108) // )
                    {
                        Avanzar();
                        if (token == 104) // ;
                        {
                            Avanzar();
                        }
                        else
                        {
                            tituloSintaxis = "Error de fin de linea en impresión 518";
                            mensajeSintaxis = "Se espera ';' en renglón ";
                            MensajeSintaxis();
                        }
                        Postfijo();
                        SemanticaToPolish();
                    }
                    else
                    {
                        tituloSintaxis = "Error de impresión 543";
                        mensajeSintaxis = "Se espera ')' en renglón ";
                        MensajeSintaxis();
                    }
                }
                else
                {
                    tituloSintaxis = "Error de impresión 542";
                    mensajeSintaxis = "Se espera un identificador, número o cadena en renglón ";
                    MensajeSintaxis();
                }
            }
            else
            {
                tituloSintaxis = "Error de impresión 541";
                mensajeSintaxis = "Se espera '(' en renglón ";
                MensajeSintaxis();
            }
        }

        public void AsignacionStmt() //Código ASIGNACIÓN
        {
            if (token == 124) //  :=
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                ExpresionAditiva();
                Postfijo();
                EjecutarSistemaDeTipos();
                SemanticaToPolish();
                if (token == 104) // ;
                {
                    Avanzar();
                }
                else
                {
                    tituloSintaxis = "Error de fin de linea de asignación";
                    mensajeSintaxis = "Se espera ';' en renglón ";
                    MensajeSintaxis();
                }
            }
            else
            {
                tituloSintaxis = "Error de asignación 544";
                mensajeSintaxis = "Se espera ':=' para hacer igualación en renglón ";
                MensajeSintaxis();
            }
        }
        #endregion

        public void EjecutarSintaxis(bool sintaxis)
        {
            contador = 0;
            msg = 0;
            listaDeNodos.Clear();
            cabeza = null;
            p = null;
            ValidarLéxico();
            token = listaDeNodos[contador].token;
            renglon = listaDeNodos[contador].renglon;
            listaDeVariables.Clear();
            EstructuraPrincipal(sintaxis);
        }

        public void EstructuraPrincipal(bool sintaxis) //Estructura Principal
        {
            try
            {
                if (token == 209) //Package
                {
                    Avanzar(); ;
                    if (token == 210) //Main
                    {
                        Avanzar();
                        if (token == 212)//var
                        {
                            while (token == 212) //una o mas var
                            {
                                Avanzar();
                                Var_Declaration(); //Var_Declaration
                            }
                            if (token == 211)//func
                            {
                                Avanzar();
                                if (token == 210)//main
                                {
                                    Avanzar();
                                    if (token == 107)// (
                                    {
                                        Avanzar();
                                        if (token == 108)// )
                                        {
                                            Avanzar();
                                            if (token == 109)// {
                                            {
                                                Avanzar();
                                                while (token != 110 && msg == 0)
                                                {
                                                    if (msg > 0)
                                                    {
                                                        contador = listaDeNodos.Count() - 1;
                                                    }
                                                    if (listaDeNodos[contador].sig == null)
                                                    {
                                                        break;
                                                    }
                                                    else
                                                        Block();
                                                }
                                                if (token == 110)// }
                                                {
                                                    NodoPolish EoF = new NodoPolish("EoF", flagPolish, token);
                                                    listaPolish.Add(EoF);
                                                    if (flagPolish != null)
                                                        flagPolish = null;
                                                    if (msg == 0 && sintaxis == true)
                                                    {
                                                        tituloSintaxis = "Listo";
                                                        mensajeSintaxis = "Sintaxis Correcta";
                                                        MessageBox.Show(this, mensajeSintaxis, tituloSintaxis, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                                    }
                                                    contador = 0;
                                                }
                                                else
                                                {
                                                    tituloSintaxis = "Error de finalización 515";
                                                    mensajeSintaxis = "Se espera '}' en renglón ";
                                                    MensajeSintaxis();
                                                }
                                            }
                                            else
                                            {
                                                tituloSintaxis = "Error de inicialización 514";
                                                mensajeSintaxis = "Se espera '{' en renglón ";
                                                MensajeSintaxis();
                                            }
                                        }
                                        else
                                        {
                                            tituloSintaxis = "Error de inicialización 513";
                                            mensajeSintaxis = "Se espera ')' en renglón ";
                                            MensajeSintaxis();
                                        }
                                    }
                                    else
                                    {
                                        tituloSintaxis = "Error de inicialización 512";
                                        mensajeSintaxis = "Se espera '(' en renglón ";
                                        MensajeSintaxis();
                                    }
                                }
                                else
                                {
                                    tituloSintaxis = "Error de inicialización 511";
                                    mensajeSintaxis = "Se espera la palabra 'main' en renglón ";
                                    MensajeSintaxis();
                                }
                            }
                            else
                            {
                                tituloSintaxis = "Error de inicialización 510";
                                mensajeSintaxis = "Se espera la palabra 'func' en renglón ";
                                MensajeSintaxis();
                            }
                        }
                        else
                        {
                            tituloSintaxis = "Error de declaración de variables 509";
                            mensajeSintaxis = "Se espera la palabra 'var' en renglón ";
                            MensajeSintaxis();
                        }
                    }
                    else
                    {
                        tituloSintaxis = "Error de inicio 508";
                        mensajeSintaxis = "Se espera la palabra 'main' en renglón ";
                        MensajeSintaxis();
                    }
                }
                else
                {
                    tituloSintaxis = "Error de inicio 507";
                    mensajeSintaxis = "Se espera iniciar con palabra 'package' en renglón ";
                    MensajeSintaxis();
                }
            }
            catch (Exception)
            {
                tituloSintaxis = "Error en Estructura Principal";
                mensajeSintaxis = "Error fatal en Estructura Principal ";
                MensajeSintaxis();
            }
        }

        #region Expresiones y Operaciones

        #region <Expresion_Lógica>
        public void Expresion_Logica()
        {
            if (token == 205 || token == 206) // Valor Logico; CASO 1
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                Expresion_Logica_1();
            }
            else
            {
                if (token == 100) // id; CASO 2
                {
                    if (BuscarVariableEnLista(listaDeNodos[contador].lexema) == true)
                    {
                        Expresion_Relacional();
                        Expresion_Logica_1();
                    }
                    else
                    {
                        tituloSemantica = "Variable No Declarada 558";
                        mensajeSemantica = "Falta declarar la variable" + listaDeNodos[contador].lexema + " en renglón ";
                        MensajeSemantica(listaDeNodos[contador].lexema);
                    }
                }
                else
                {
                    if (token == 107) // (; CASO 3
                    {
                        GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                        Avanzar();
                        Expresion_Logica();
                        Expresion_Logica_1();
                        if (token == 108) // )
                        {
                            GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                            Avanzar();
                            Expresion_Logica_1();
                        }
                        else
                        {
                            tituloSintaxis = "Error de Expresión Lógica 545";
                            mensajeSintaxis = "Se espera ')' en renglón ";
                            MensajeSintaxis();
                        }
                    }
                    else
                    {
                        if (token == 128) //! ;  CASO 4
                        {
                            GuardarPolish(listaDeNodos[contador].lexema, 19, renglon);
                            Avanzar();
                            Expresion_Logica();
                            Expresion_Logica_1();
                        }
                        else
                        {
                            Expresion_Relacional(); //CASO 1
                            Expresion_Logica_1();
                        }
                    }
                }
            }
        }

        public void Expresion_Logica_1()
        {
            if (token == 126 || token == 127)  // Operador_Lógico_2, &&  ||
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                Expresion_Logica();
                Expresion_Logica_1();
            }
        }

        public void Expresion_Relacional()
        {
            Expresion_Numerica();
            if (token == 120 || token == 121 || token == 122 || token == 123 || token == 118 || token == 119) // Operador_Relacional
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                Expresion_Numerica();
            }
            else
            {
                tituloSintaxis = "Error de Expresión Lógica. Expresión Relacional. 546";
                mensajeSintaxis = "Se espera Operador Relacional en renglón ";
                MensajeSintaxis();
            }
        }

        public void Expresion_Numerica_1()
        {
            if (token == 113 || token == 114 || token == 115 || token == 116) //Operador numérico; + - * /
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                Expresion_Numerica();
                Expresion_Numerica_1();
            }
        }

        public void Expresion_Numerica()
        {
            if (token == 107) // (; CASO 1    
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                Expresion_Numerica();
                if (token == 108) // )
                {
                    GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                    Avanzar();
                    Expresion_Numerica_1();
                }
            }
            else
            {
                if (token == 114) // -  ; CASO 2
                {
                    GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                    Avanzar();
                    Expresion_Numerica();
                    Expresion_Numerica_1();
                }
                else
                {
                    if (token == 100) //id; CASO 3
                    {
                        if (BuscarVariableEnLista(listaDeNodos[contador].lexema) == true)
                        {
                            GuardarPolish(listaDeNodos[contador].lexema, tipoVariable, renglon);
                            Avanzar();
                            Expresion_Numerica_1();
                        }
                        else
                        {
                            tituloSemantica = "Variable No Declarada 558";
                            mensajeSemantica = "Falta declarar la variable" + listaDeNodos[contador].lexema + " en renglón ";
                            MensajeSemantica(listaDeNodos[contador].lexema);
                        }
                    }
                    else
                    {
                        if (token == 101 || token == 102) // valor_numerico ; CASO 4
                        {
                            GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                            Avanzar();
                            Expresion_Numerica_1();
                        }
                        else
                        {
                            if (token == 205 || token == 206) // valor_logico ; CASO 5
                            {
                                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                                Avanzar();
                                Expresion_Numerica_1();

                            }
                            else
                            {
                                if (token == 125) // valor_cadena ; CASO 6
                                {
                                    GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                                    Avanzar();
                                    Expresion_Numerica_1();

                                }
                                else
                                {
                                    tituloSintaxis = "Error de Expresión Lógica. Expresión Numérica. 547";
                                    mensajeSintaxis = "Se espera Expresión Numérica válida en renglón ";
                                    MensajeSintaxis();
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion //<Expresion_Lógica>

        #region <Expresion_Aditiva>
        public void ExpresionAditiva()
        {
            if (token == 107)
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                ExpresionAditiva();
                if (token == 113 || token == 114) //Operador_Aditivo; CASO 2
                {
                    GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                    Avanzar();
                    ExpresionMultiplicativa();
                }
                else if (token != 104 && token != 108) // ;
                {
                    ExpresionAditiva();
                }
                else
                {
                    ExpresionMultiplicativa(); // CASO 1
                }
                if (token != 104 && token != 108) // ;
                {
                    ExpresionAditiva();
                }
                if (token == 108)
                {
                    GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                    Avanzar();
                    if (token != 104)
                    {
                        ExpresionAditiva();
                    }
                }
            }
            else if (token == 108)
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
            }
            else
            {
                if (token == 113 || token == 114) //Operador_Aditivo; CASO 2
                {
                    GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                    Avanzar();
                    ExpresionMultiplicativa();
                }
                else
                {
                    ExpresionMultiplicativa(); // CASO 1
                }
                if (token != 104 && token != 108) // ;
                {
                    ExpresionAditiva();
                }
            }
        }

        public void ExpresionMultiplicativa()
        {
            if (token == 107)
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                ExpresionAditiva();
            }
            else if (token == 108)
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
            }
            else
            {
                if (token == 115 || token == 116) //Operador_Multiplicativo; CASO 2
                {
                    GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                    Avanzar();
                    ExpresionUnitaria();
                }
                else
                {
                    ExpresionUnitaria(); //CASO 1
                }
            }
        }

        public void EjecutarSemantica(bool semantica)
        {
            pilaOperadores.Clear();
            pilaOperandos.Clear();
            listaDeNodos.Clear();
            listaDeVariables.Clear();
            listaSemanticaOperaciones.Clear();
            listaTemporalOperadores.Clear();
            listaPolish.Clear();
            contadorFor = 0;
            contadorIf = 0;
            EjecutarSintaxis(semantica);
            if (mesSem == 0)
            {
                MessageBox.Show(this, "Sin errores", "Semantica Correcta");
            }
        }

        public void ExpresionUnitaria()
        {
            if (token == 107) //(
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
                ExpresionAditiva();
            }
            else if (token == 108)
            {
                GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                Avanzar();
            }
            else
            {
                if (token == 100 || token == 101 || token == 102 || token == 125 || token == 205 || token == 206) //Expresion Primaria; CASO 1
                {
                    if (BuscarVariableEnLista(listaDeNodos[contador].lexema) == true)
                    {
                        GuardarPolish(listaDeNodos[contador].lexema, tipoVariable, renglon);
                    }
                    else
                    {
                        if (token != 100)
                        {
                            GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                        }
                        else
                        {
                            tituloSemantica = "Variable no declarada";
                            mensajeSemantica = "Variable no declarada en el renglón ";
                            MensajeSemantica(listaDeNodos[contador].lexema);
                        }
                    }
                    Avanzar();
                }
                else
                {
                    if (token == 113 || token == 114) //Operador Unitario, CASO 2
                    {
                        GuardarPolish(listaDeNodos[contador].lexema, token, renglon);
                        Avanzar();
                        ExpresionUnitaria();
                    }
                    else
                    {
                        tituloSintaxis = "Error de asignación 548";
                        mensajeSintaxis = "Se espera Expresión Unitaria (Expresion_Primaria u Operador_Unitario) en renglón ";
                        MensajeSintaxis();
                    }
                }
            }
        }
        #endregion  // <Expresion_Aditiva>
        #endregion //Expresiones y Operaciones

        public void MensajeSintaxis()
        {
            try
            {
                int renglonMensaje = renglon;
                string lexemaMensaje = listaDeNodos[contador].lexema;
                if (contador > 0)
                {
                    if ((renglon > listaDeNodos[contador - 1].renglon && listaDeNodos[contador - 1].token != 104 && listaDeNodos[contador - 1].token != 107 && listaDeNodos[contador - 1].token != 109 && listaDeNodos[contador - 1].token != 110) && contador > 1)
                    {
                        renglonMensaje = listaDeNodos[contador - 1].renglon;
                        lexemaMensaje = listaDeNodos[contador - 1].lexema;
                    }
                }
                if (msg < 1)
                {
                    MessageBox.Show(this, mensajeSintaxis + renglonMensaje + " en: " + lexemaMensaje, tituloSintaxis, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                msg++;
            }
            catch (Exception)
            {
                contador = 1;
            }
        }

        public void Var_Declaration() //Var_Declaration
        {
            if (token == 100)
            {
                if (BuscarVariableEnLista(listaDeNodos[contador].lexema) == false)
                {
                    pilaVariablesSemantica.Add(listaDeNodos[contador].lexema); //Guarda variables antes de saber el tipo
                    Avanzar();
                }
                else
                {
                    tituloSemantica = "Variable Multideclarada 549";
                    mensajeSemantica = "Se nombró una variable con el mismo nombre de una ya existente en renglón ";
                    MensajeSemantica(listaDeNodos[contador].lexema);
                }
                switch (token)
                {
                    case 105: // ,
                        Avanzar();
                        Var_Declaration();
                        break;

                    default:
                        Tipo();
                        pilaVariablesSemantica.Clear();
                        if (token == 104) // ;
                        {
                            Avanzar();
                        }
                        else
                        {
                            tituloSintaxis = "Error de fin de linea 518";
                            mensajeSintaxis = "Se espera ';' en renglón ";
                            MensajeSintaxis();
                        }
                        break;
                }
            }
            else
            {
                tituloSintaxis = "Error de declaración de variables 516";
                mensajeSintaxis = "Se espera identificador en renglón ";
                MensajeSintaxis();
            }
        }

        public void Tipo() //Type
        {
            switch (token)
            {
                case 215: //int
                    tokenTipo = 1;
                    break;

                case 216: //string
                    tokenTipo = 3;
                    break;

                case 217: //bool
                    tokenTipo = 4;
                    break;

                case 218: //float
                    tokenTipo = 2;
                    break;

                case 220: //char
                    tokenTipo = 3;
                    break;

                default:
                    tituloSintaxis = "Error de declaración de variables 517";
                    mensajeSintaxis = "Se espera el tipo de la(s) variable(s) en renglón ";
                    MensajeSintaxis();
                    break;
            }
            for (int i = 0; i < pilaVariablesSemantica.Count; i++)
            {
                GuardarVariables(pilaVariablesSemantica[i], tokenTipo);
            }
            Avanzar();
        }
        #endregion

        #region Lista De Polish
        public void GuardarPolish(string lexema, int token, int renglon)
        {
            Nodo nodo = new Nodo(lexema, token, renglon);
            if (cabeza == null)
            {
                cabeza = nodo;
                p = nodo;
                listaTemporalOperadores.Add(cabeza);
            }
            else
            {
                p.sig = nodo;
                p = nodo;
                listaTemporalOperadores.Add(nodo);
            }
        }
        #endregion

        #region ASM
        public void AddStringToListForASM(string cadena, List<string> lista)
        {
            bool found = false;
            foreach (string item in lista)
            {
                if (cadena == item)
                {
                    found = true;
                    break;
                }
            }
            if (found == false)
            {
                lista.Add(cadena);
            }
        }
        #endregion
    }
}