﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompiladorGo_MatrizDeTransición
{
    class Nodo
    {
        public Nodo(string lexema, int token, int renglon)
        {
            this.lexema = lexema;
            this.token = token;
            this.renglon = renglon;
        }
        public string lexema;
        public int token;
        public int renglon;
        public Nodo sig = null;
    }
}
