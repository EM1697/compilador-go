﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text;


namespace CompiladorGo_MatrizDeTransición
{
    class Ensamblador
    {
        #region Constructor
        public Ensamblador() { }
        #endregion

        #region Variables
        public List<string> listaStrings = new List<string>();
        public List<string> listaNumeros = new List<string>();
        public List<NodoPolish> listaPolish = new List<NodoPolish>();
        public List<Nodo> listaVariables = new List<Nodo>();
        int operandos = 0;
        int operadores = 0;
        Stack<NodoPolish> pila = new Stack<NodoPolish>();
        #region Código Ensamblador
        private string codeASM = null;
        private string header = null;
        private string precode = null;
        public string code = null;
        private string postcode = null;
        #endregion        #region Form1
        #endregion

        #region Métodos
        #region Crear Variables ASM
        private void VarCadenas()
        {
            foreach (string item in listaStrings)
            {
                string i = item.Replace(' ', '_');
                header += "\t\t\t" + i + " db '" + item + "','$' \n\r";
            }
        }

        private void VarNumeros()
        {
            foreach (string item in listaNumeros)
            {
                if (item.IndexOf('.') == -1)
                    header += "\t\t\tOP" + item + " dw " + item + " \n\r";
                else
                {
                    string i = item;
                    i = i.Replace('.', 'p');
                    header += "\t\t\tOP" + i + " dd " + item + "\n\r";
                }
            }
        }

        private void Variables()
        {
            foreach (Nodo item in listaVariables)
            {
                if (item.token == 3)
                    header += "\t\t\t" + item.lexema + " db '','$' \n\r";
                else if (item.token == 2)
                    header += "\t\t\t" + item.lexema + " dw ?\n\r";
                else
                    header += "\t\t\t" + item.lexema + " dw ?\n\r";
            }
        }
        #endregion

        private void Header()
        {
            header = "INCLUDE macros.mac\n\rDOSSEG\n\r.MODEL SMALL\n\rSTACK 100h\n\r.DATA\n\r\t\t\t" +
                        "BUFFER      DB 8 DUP('$');23h\n\r\t\t\tBUFFERTEMP  DB 8 DUP('$');23h\n\r\t\t\t" +
                        "BLANCO  DB '#'\n\r\t\t\tBLANCOS DB '$'\n\r\t\t\tMENOS DB '-$'\n\r\t\t\tCOUNT DW 0\n\r\t\t\t" +
                        "NEGATIVO DB 0\n\r\t\t\tARREGLO DW 0\n\r\t\t\tARREGLO1 DW 0\n\r\t\t\tARREGLO2 DW 0\n\r\t\t\t" +
                        "LISTAPAR LABEL BYTE\n\r\t\t\tLONGMAX DB 254\n\r\t\t\tTOTCAR DB ?\n\r\t\t\t" +
                        "INTRODUCIDOS DB 254 DUP('$')\n\r\t\t\tMULT10 DW 1\n\r\t\t\tt1 dw ?\n\r" +
                        "\t\t\tt2 dw ? \n\r\t\t\tt3 dw ? \n\r\t\t\tt4 dw ? \n\r\t\t\tt5 dw ? \n\r\t\t\ttrue db 'true','$'\n\r\t\t\tfalse db 'false','$'\n\r";
            VarCadenas();
            VarNumeros();
            Variables();
            codeASM = header;
        }

        private void Precode()
        {
            precode = ".CODE\n\r.386\nBEGIN:\n\r\t\t\tMOV AX, @DATA\n\r\t\t\tMOV DS, AX\n\rCALL COMPI\n\r\t\t\t" +
                      "MOV AX, 4C00H\n\r\t\t\tINT 21H\n\rCOMPI  PROC\n\r";
            codeASM += precode;
        }

        #region Code
        public void PrintCode(int tokenImprimir, string line)
        {
            string variable = line.Replace(' ', '_');
            if (operadores == 0)
            {
                if (tokenImprimir == 101 || tokenImprimir == 102 || tokenImprimir == 1 || tokenImprimir == 2)
                {
                    if (line.IndexOf('.') == -1)
                    {
                        if (line.Length == 1 && Char.IsLetter(line[0]))
                        {
                            code += "\t\titoa buffer," + line + "\n\r\t\twritenum buffer\n\r\t\tborrar buffer, blancos\n\r\t\tborrar t1, blancos\n\r\t\twriteln\n\r";
                        }
                        else
                        {
                            code += "\t\titoa buffer, " + line + "\n\r\t\twritenum buffer\n\r\t\tborrar buffer, blancos\n\r\t\tborrar t1, blancos\n\r\t\twriteln\n\r";
                        }
                    }
                    else
                    {
                        string i = line;
                        i = i.Replace('.', 'p');
                        code += "\t\twrite OP" + i + " \n\r\t\twriteln\n\r";
                    }
                }
                else
                    code += "\t\twrite " + variable.Trim('"') + " \n\r\t\twriteln\n\r";
            }
            else
            {
                if (tokenImprimir == 101 || tokenImprimir == 102 || tokenImprimir == 1 || tokenImprimir == 2)
                {
                    if (line.IndexOf('.') == -1)
                    {
                        if (line.Length == 1 && Char.IsLetter(line[0]))
                        {
                            code += "\t\titoa buffer," + line + "\n\r\t\twrite buffer\n\r\t\tborrar buffer, blancos\n\r\t\twriteln\n\r";
                        }
                        else
                        {
                            code += "\t\titoa buffer, " + line + "\n\r\t\twrite buffer\n\r\t\tborrar buffer, blancos\n\r\t\twriteln\n\r";
                        }
                    }
                    else
                    {
                        string i = line;
                        i = i.Replace('.', 'p');
                        code += "\t\twrite OP" + i + " \n\r\t\twriteln\n\r";
                    }
                }
                else
                    code += "\t\twrite " + variable.Trim('"') + " \n\r\t\twriteln\n\r";
            }
        }

        private void Code()
        {
            for (int i = 0; i < listaPolish.Count; i++)
            {
                try
                {
                    NodoPolish item = listaPolish[i];
                    if (item.lexema == "+" && item.token == 101)
                    {
                        item.token = 5;
                    }
                    else if (item.lexema == ":=" && item.token == 101)
                    {
                        item.token = 13;
                    }
                    #region Apuntadores
                    if (item.flag != null)
                    {
                        for (int e = 0; e < item.flag.Length; e++)
                        {
                            if (Char.IsLetter(item.flag[e]))
                            {
                                code += "\t\t" + item.flag[e] + item.flag[e + 1] + ":\n\r";
                                operandos = 0;
                            }
                        }
                    }
                    #endregion
                    if (item.token == 1 || item.token == 2 || item.token == 3 || item.token == 101 || item.token == 102 || item.token == 4 || item.token == 205 || item.token == 206)
                    {
                        operandos++;
                        pila.Push(item);
                    }
                    else if (item.token != 109 && item.token != 110 && item.token != 104)
                    {
                        #region Print
                        if (item.lexema == "print")
                        {
                            NodoPolish item_1 = pila.Pop();
                            PrintCode(item_1.token, item_1.lexema);
                        }
                        #endregion
                        else if (i > 1 && item.lexema != "EoF")
                        {
                            NodoPolish op2 = pila.Pop();
                            string b = op2.lexema.Replace('.', 'p');
                            string variable2 = op2.lexema.Replace(' ', '_');
                            #region Asignación
                            if (item.lexema == ":=")
                            {
                                NodoPolish op1 = pila.Pop();
                                string a = op1.lexema.Replace('.', 'p');
                                string variable1 = op1.lexema.Replace(' ', '_');
                                if (operadores == 0)
                                {
                                    if (op1.token == 3 || op1.token == 4)
                                        code += "\t\tborrar " + op1.lexema + ", blancos\n\r\t\ts_asignar " + op1.lexema + ", " + variable2.Trim('"') + "\n\r";
                                    else
                                    {
                                        if (op2.lexema == "true")
                                        {
                                            code += "\t\tborrar " + op1.lexema + ", blancos\n\r\t\ts_asignar " + op1.lexema + ", true\n\r";
                                        }
                                        else if (op2.lexema == "false")
                                        {
                                            code += "\t\tborrar " + op1.lexema + ", blancos\n\r\t\ts_asignar " + op1.lexema + ", false\n\r";
                                        }
                                        else
                                        {

                                            code += "\t\tborrar " + op1.lexema + ", blancos\n\r\t\ti_asignar " + op1.lexema + ", " + op2.lexema + "\n\r";
                                        }
                                    }
                                }
                                else
                                {
                                    if (op1.token == 3 || op1.token == 4)
                                        code += "\t\tborrar " + op1.lexema + ", blancos\n\r\t\ts_asignar " + listaPolish[i - operandos].lexema + ", t1\n\r";
                                    else
                                    {
                                        if (op2.lexema == "true")
                                        {
                                            code += "\t\tborrar " + op1.lexema + ", blancos\n\r\t\ts_asignar " + op1.lexema + ", true\n\r";
                                        }
                                        else if (op2.lexema == "false")
                                        {
                                            code += "\t\tborrar " + op1.lexema + ", blancos\n\r\t\ts_asignar " + op1.lexema + ", false\n\r";
                                        }
                                        else
                                        {

                                            code += "\t\tborrar " + op1.lexema + ", blancos\n\r\t\ti_asignar " + op1.lexema + ", " + op2.lexema + "\n\r";
                                        }
                                    }
                                }
                                operadores = 0;
                                pila.Clear();
                            }
                            #endregion
                            #region Operaciones Aritméticas y Relacionales
                            else
                            {
                                NodoPolish op1 = pila.Pop();
                                string a = op1.lexema.Replace('.', 'p');
                                string variable1 = op1.lexema.Replace(' ', '_');
                                int tokenTemp = op1.token;
                                #region Switch Operadores
                                switch (item.token)
                                {
                                    #region Aritméticos
                                    #region Suma
                                    case 5: // +
                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (Char.IsDigit(op1.lexema[0]))
                                                op1.lexema = "op" + op1.lexema;
                                            if (Char.IsDigit(op2.lexema[0]))
                                                op2.lexema = "op" + op2.lexema;
                                            if (operadores == 0)
                                            {
                                                code += "\t\tsumar " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                tokenTemp = 1;
                                            }
                                            else if (operadores == 1)
                                            {
                                                code += "\t\tsumar " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                tokenTemp = 1;
                                            }
                                            else
                                            {
                                                code += "\n\r\t\tsumar " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                tokenTemp = 1;
                                            }

                                        }
                                        else if (op1.token == 2 || op1.token == 102)
                                        {
                                            if (Char.IsDigit(op1.lexema[0]))
                                                op1.lexema = "op" + op1.lexema;
                                            if (Char.IsDigit(op2.lexema[0]))
                                                op2.lexema = "op" + op2.lexema;
                                            if (operadores == 0)
                                            {
                                                code += "\t\tsumar " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                tokenTemp = 2;
                                            }
                                            else if (operadores == 1)
                                            {
                                                code += "\t\tsumar " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                tokenTemp = 2;
                                            }
                                            else
                                            {
                                                code += "\n\r\t\tsumar " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                tokenTemp = 3;
                                            }
                                        }
                                        else if (op1.token == 3)
                                        {
                                            code += "\t\tconcatenar " + variable1 + ", " + variable2 + ", t1\n\r";
                                            tokenTemp = 3;
                                        }
                                        NodoPolish nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    #region Resta
                                    case 6: // -
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (operadores == 0)
                                            {
                                                code += "\t\tresta " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                tokenTemp = 2;
                                            }
                                            else if (operadores == 1)
                                            {
                                                code += "\t\tresta " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                tokenTemp = 2;
                                            }
                                            else
                                            {
                                                code += "\n\r\t\tresta " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                tokenTemp = 2;
                                            }

                                        }
                                        else if (op1.token == 2 || op1.token == 102)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tresta " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tresta " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tresta " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tresta " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tresta " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tresta " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                        }
                                        nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    #region Multiplicación
                                    case 7: // *
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tmulti " + op1.lexema + ", " + b + ", t1\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tmulti " + op1.lexema + ", " + b + ", t2\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tmulti " + op1.lexema + ", " + b + ",t3\n\r";
                                                    tokenTemp = 2;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tmulti " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tmulti " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tmulti " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }

                                        }
                                        else if (op1.token == 102 || op1.token == 2)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tmulti " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tmulti " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tmulti " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 2;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tmulti " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tmulti " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tmulti " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 2;
                                                }
                                            }
                                        }
                                        nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    #region División
                                    case 8: // /
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tdivide " + op1.lexema + ", " + b + ", t1\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tdivide " + op1.lexema + ", " + b + ", t2\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tdivide " + op1.lexema + ", " + b + ",t3\n\r";
                                                    tokenTemp = 2;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tdivide " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tdivide " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tdivide " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 2;
                                                }
                                            }

                                        }
                                        else if (op1.token == 102 || op1.token == 2)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tdivide " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tdivide " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tdivide " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 2;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\tdivide " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\tdivide " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 2;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\tdivide " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 2;
                                                }
                                            }
                                        }
                                        nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    #endregion

                                    //#region Relacionales
                                    #region <
                                    case 9: // <
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_menor " + op1.lexema + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_menor " + op1.lexema + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_menor " + op1.lexema + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_menor " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_menor " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_menor " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }

                                        }
                                        else if (op1.token == 102 || op1.token == 2)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_menor " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_menor " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_menor " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_menor " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_menor " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_menor " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                        }
                                        nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    #region >
                                    case 10: // >
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_mayor " + op1.lexema + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_mayor " + op1.lexema + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_mayor " + op1.lexema + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_mayor " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_mayor " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_mayor " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }

                                        }
                                        else if (op1.token == 102 || op1.token == 2)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_mayor " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_mayor " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_mayor " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_mayor " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_mayor " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_mayor " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                        }
                                        nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    #region <=
                                    case 11: // <=
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_menorigual " + op1.lexema + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_menorigual " + op1.lexema + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_menorigual " + op1.lexema + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_menorigual " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_menorigual " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_menorigual " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }

                                        }
                                        else if (op1.token == 102 || op1.token == 2)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_menorigual " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_menorigual " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_menorigual " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_menorigual " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_menorigual " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_menorigual " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                        }
                                        nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    #region >=
                                    case 12: // >=
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_mayorigual " + op1.lexema + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_mayorigual " + op1.lexema + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_mayorigual " + op1.lexema + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_mayorigual " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_mayorigual " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_mayorigual " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }

                                        }
                                        else if (op1.token == 102 || op1.token == 2)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_mayorigual " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_mayorigual " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_mayorigual " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_mayorigual " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_mayorigual " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_mayorigual " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                        }
                                        nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    #region &&, ||, -, !
                                    //case 14: // &&
                                    //    if (op1 == 4 && op2 == 4)
                                    //    {
                                    //        resultado = 4;
                                    //    }
                                    //    else
                                    //    {
                                    //        tituloSemantica = "Variable fuera de contexto";
                                    //        mensajeSemantica = "Operando inválido en renglón ";
                                    //    }
                                    //    break;

                                    //case 15: // ||
                                    //    if (op1 == 4)
                                    //    {
                                    //        if (op2 == 4)
                                    //        {
                                    //            resultado = 4;
                                    //        }
                                    //        else
                                    //        {
                                    //            tituloSemantica = "Variable fuera de contexto";
                                    //            mensajeSemantica = "Operando inválido en renglón ";
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        tituloSemantica = "Variable fuera de contexto";
                                    //        mensajeSemantica = "Operando inválido en renglón ";
                                    //    }
                                    //    break;

                                    //case 18: // @
                                    //    if (op1 == 4)
                                    //    {
                                    //        resultado = 4;
                                    //    }
                                    //    else
                                    //    {
                                    //        tituloSemantica = "Variable fuera de contexto";
                                    //        mensajeSemantica = "Operando inválido en renglón ";
                                    //    }
                                    //    break;

                                    //case 19: // !
                                    //    if (op1 == 4)
                                    //    {
                                    //        resultado = 4;
                                    //    }
                                    //    else
                                    //    {
                                    //        tituloSemantica = "Variable fuera de contexto";
                                    //        mensajeSemantica = "Operando inválido en renglón ";
                                    //    }
                                    //    break; 
                                    #endregion

                                    #region ==
                                    case 20: // ==
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_igual " + op1.lexema + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_igual " + op1.lexema + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_igual " + op1.lexema + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_igual " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_igual " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_igual " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }

                                        }
                                        else if (op1.token == 102 || op1.token == 2)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_igual " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_igual " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_igual " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_igual " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_igual " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_igual " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                        }
                                        else if (op1.token == 3)
                                        {
                                            code += "\t\ti_igual " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                            tokenTemp = 1;
                                        }
                                        pila.Clear();
                                        break;
                                    #endregion

                                    #region !=
                                    case 21: // !=
                                        if (Char.IsDigit(op1.lexema[0]))
                                            op1.lexema = "op" + op1.lexema;
                                        if (Char.IsDigit(op2.lexema[0]))
                                            op2.lexema = "op" + op2.lexema;

                                        if (op1.token == 101 || op1.token == 1)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_diferentes " + op1.lexema + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_diferentes " + op1.lexema + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_diferentes " + op1.lexema + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_diferentes " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_diferentes " + op1.lexema + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_diferentes " + op1.lexema + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }

                                        }
                                        else if (op1.token == 102 || op1.token == 2)
                                        {
                                            if (op2.token == 2 || op2.token == 102)
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_diferentes " + a + ", " + b + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_diferentes " + a + ", " + b + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_diferentes " + a + ", " + b + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                            else
                                            {
                                                if (operadores == 0)
                                                {
                                                    code += "\t\ti_diferentes " + a + ", " + op2.lexema + ", t1\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else if (operadores == 1)
                                                {
                                                    code += "\t\ti_diferentes " + a + ", " + op2.lexema + ", t2\n\r";
                                                    tokenTemp = 1;
                                                }
                                                else
                                                {
                                                    code += "\n\r\t\ti_diferentes " + a + ", " + op2.lexema + ",t3\n\r";
                                                    tokenTemp = 1;
                                                }
                                            }
                                        }
                                        else if (op1.token == 3)
                                        {
                                            code += "\t\ti_diferentes " + op1.lexema + ", " + op2.lexema + ", t1\n\r";
                                            tokenTemp = 1;
                                        }
                                        nodo = new NodoPolish("T" + (operadores + 1).ToString(), null, tokenTemp);
                                        pila.Push(nodo);
                                        break;
                                    #endregion

                                    default:
                                        break;
                                }
                                #endregion
                                if (item.token >= 5 && item.token <= 8)
                                    operadores++;
                            }

                            #endregion
                        }
                        else if (item.lexema.Substring(0, 4) == "Br-F")
                        {
                            code += "\t\tjf t1, " + item.lexema.Substring(5, 2) + "\n\r";
                        }
                        else if (item.lexema.Substring(0, 4) == "Br-I")
                        {
                            code += "\t\tjmp " + item.lexema.Substring(5, 2) + "\n\r";
                        }
                    }
                    else if (item.lexema != "EoF")
                    {
                        if (item.lexema.Substring(0, 4) == "Br-F")
                        {
                            code += "\t\tjf t1, " + item.lexema.Substring(5, 2) + "\n\r";
                        }
                        else if (item.lexema.Substring(0, 4) == "Br-I")
                        {
                            code += "\t\tjmp " + item.lexema.Substring(5, 2) + "\n\r";
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            codeASM += code;
        }

        public void Code(string line)
        {
            code += "\t" + line + "\n\r";
        }
        #endregion

        private void Postcode()
        {
            postcode = "\t\tret\n\rCOMPI  ENDP\n\rEND BEGIN";
            codeASM += postcode;
        }

        public void Compilar()
        {
            header = null;
            precode = null;
            postcode = null;
            codeASM = null;
            operandos = 0;
            pila.Clear();
            Header();
            Precode();
            Code();
            Postcode();
            CreateFile();
        }

        private void CreateFile()
        {
            string path = @"C:\8086\go.asm";
            System.IO.File.WriteAllText(path, string.Empty);
            System.IO.File.WriteAllText(path, codeASM);
        }
        #endregion
    }
}
